from dbus_next.aio import MessageBus
from dbus_next import Variant
from agents import AudioAgent
import signal
import consts

import asyncio

AGENT = '/audiopi/agent'
SYSTEM_NAME = 'pi2'

loop = asyncio.get_event_loop()

process = None;

def die(signum, frame):
    if process:
        process.set_result(True)
    print('Audio Pi Agent closed')

signal.signal(signal.SIGTERM, die)

async def get_proxies(bus):
    device_introspection = await bus.introspect(consts.BLUEZ, '/org/bluez/hci0')
    bluez_introspection = await bus.introspect(consts.BLUEZ, '/org/bluez')

    device_proxy = bus.get_proxy_object(
        consts.BLUEZ, '/org/bluez/hci0', device_introspection)
    bluez_proxy = bus.get_proxy_object(
        consts.BLUEZ, '/org/bluez', bluez_introspection)

    i_bluez = device_proxy.get_interface(consts.BLUEZ_1_ADAPTER)
    i_agent = bluez_proxy.get_interface(consts.BLUEZ_1_AGENTMANAGER)

    properties = device_proxy.get_interface('org.freedesktop.DBus.Properties')
    
    def on_properties_changed(interface_name, changed_properties, invalidated_properties):
        for changed, variant in changed_properties.items():
            print(f'property changed: {changed} - {variant.value}')

    properties.on_properties_changed(on_properties_changed)

    return (i_bluez, i_agent)


async def device_setup(proxy):
    await proxy.set_alias(SYSTEM_NAME)
    await proxy.set_discoverable_timeout(60)  # no timeout for discovery
    await proxy.set_pairable_timeout(60)  # 60 seconds for pairing timeout
    await proxy.set_discoverable(False)  
    await proxy.set_pairable(False) 


async def register_agent(proxy, bus):
    auth_agent = AudioAgent(consts.BLUEZ_1_AGENT, bus)
    bus.export(AGENT, auth_agent)
    bus_obj = await bus.request_name(consts.BLUEZ_1_AGENT)

    await proxy.call_register_agent(AGENT, 'NoInputNoOutput')
    await proxy.call_request_default_agent(AGENT)

async def main():
    global process
    bus = await MessageBus().connect()

    i_bluez, i_agent = await get_proxies(bus)

    await device_setup(i_bluez)
    await register_agent(i_agent, bus)

    process = await loop.create_future()


loop.run_until_complete(main())
