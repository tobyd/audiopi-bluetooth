from dbus_next.service import ServiceInterface, method
import consts

class AudioAgent(ServiceInterface):
    def __init__(self, name, bus):
        super().__init__(name)
        self.bus = bus;

    @method(name='Release')
    def release(self):
        pass

    @method(name='RequestPinCode')
    def request_pin_code(self, device: 'o') -> 's':
        pass

    @method(name='DisplayPinCode')
    def display_pin_code(self, device: 'o', pincode: 's'):
        pass

    @method(name='RequestPasskey')
    async def request_passkey(self, device: 'o') -> 'u':
        pass

    @method(name='DisplayPasskey')
    def display_passkey(self, device: 'o', passkey: 'u', entered: 'q'):
        pass

    @method(name='RequestConfirmation')
    def request_confirmation(self, device: 'o', passkey: 'u'):
        pass

    @method(name='RequestAuthorization')
    def request_authorization(self, device: 'o'):
        pass

    @method(name='AuthorizeService')
    async def authorize_service(self, device: 'o', uuid: 's'):
        device = await self.get_device(device)
        await device.set_trusted(True)
        pass

    @method(name='Cancel')
    def cancel(self):
        pass

    async def get_device(self, device_string):
        device_introspection = await self.bus.introspect(consts.BLUEZ, device_string)
        device_proxy = self.bus.get_proxy_object(consts.BLUEZ, device_string, device_introspection)
        device_interface = device_proxy.get_interface(consts.BLUEZ_1_DEVICE)
        return device_interface


