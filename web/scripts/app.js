const requestExposeBluetooth = () => request('expose');
 
const requestPowerOn = () => request('on');

const requestPowerOff = () => request('off');

const request = (name) => {
    if (socket && socket.OPEN) {
        socket.send(name);
    }
}

const setupEvents = () => {
    document.getElementById('expose').addEventListener('click', requestExposeBluetooth);
}

let ping = 0;

const host = `${window.location.protocol === 'https:' ? 'wss://' : 'ws://'}${window.location.host}/api/socket`

const socket = new WebSocket(host);

socket.addEventListener('open', () => {
    socket.addEventListener('message', (e) => {

        if (e.data === 'pong') {
            return;
        }

        const message = JSON.parse(e)

        switch (message.context) {
            case 'device':
                break;
            default:
                break;
        }
    });

    ping = setInterval(() => {
        request('ping')
    }, 5000);

    setupEvents();
});

socket.addEventListener('close', (_) => {
    clearPing();
});

socket.addEventListener('error', (_) => {
    clearPing();
});

const clearPing = () => {
    if (ping) {
        clearInterval(ping);
    }
}


    
