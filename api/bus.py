from dbus_next.aio import MessageBus

BLUEZ = 'org.bluez'
BLUEZ_1_ADAPTER = '{}.Adapter1'.format(BLUEZ)
BLUEZ_DEVICE_PATH = '/org/bluez/hci0'


class BusHandler(object):
    def __init__(self, logger):
        self.logger = logger

    async def setup_async(self, device_path=BLUEZ_DEVICE_PATH):
        self.logger.info('Bus handler starting')
        self.bus = await MessageBus().connect()

        self.logger.info('Bus handler connected')

        device_introspection = await self.bus.introspect(BLUEZ, device_path)

        device_proxy = self.bus.get_proxy_object(
            BLUEZ, device_path, device_introspection)

        self.proxy = device_proxy.get_interface(BLUEZ_1_ADAPTER)

        self.logger.info('Proxy ready')

    async def ping_async(self):
        return 'PONG'

    async def shutdown_async(self):
        pass

    async def expose_async(self):
        await self.proxy.set_pairable(True)
        await self.proxy.set_discoverable(True)

        self.logger.info('Device exposed for pairing')

    async def disable_async(self):
        await self.power()

    async def enable_async(self):
        await self.power(False)

    async def power(self, on=True):
        current_state = await self.proxy.get_powered()
        if current_state != on:
            await self.proxy.set_powered(on)
            self.logger.info('Device powered state changed to {}'.format(on))
