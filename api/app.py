import aiohttp
from aiohttp import web
import asyncio
from bus import BusHandler
import logging
import json
from aiohttp.web import middleware

logger = logging.getLogger('AudioPi API')

app = web.Application()
app['sockets'] = [];
app['dbus'] = BusHandler(logger)

async def websocket_handler(request):

    ws = web.WebSocketResponse()
    await ws.prepare(request)

    logger.info('New websocket connection')
    request.app['sockets'].append(ws)

    bus_handler = app['dbus']

    handlers = {
        'expose': (bus_handler.expose_async, {'state': 'exposed', 'context': 'device'}),
        'on': (bus_handler.enable_async, {'state': 'on', 'context': 'device'}),
        'off': (bus_handler.disable_async, {'state': 'off', 'context': 'device'}),
        'ping': (bus_handler.ping_async, 'pong')
    }

    async for msg in ws:
        if msg.type == aiohttp.WSMsgType.TEXT:
    
            handler = handlers.get(msg.data, None)

            if handler != None:
                await handler[0]()
                await broadcast_async(handler[1])
                

        elif msg.type == aiohttp.WSMsgType.ERROR:
            print('ws connection closed with exception %s' %
                  ws.exception())

    request.app['sockets'].remove(ws)
    logger.info('Websocket connection closed');

    return ws


async def broadcast_async(message):
    if message is dict:
        payload = json.dumps(message)
    else:
        payload = message
    
    for socket in app['sockets']:
        await socket.send_str(payload)


async def setup(app):
    await app['dbus'].setup_async()

async def health_handler(request):
    return web.Response(text='ok')

app.add_routes([web.get('/', health_handler), web.get('/socket', websocket_handler)])

app.on_startup.append(setup)

web.run_app(app, host='localhost', port='8080')
